class StandardJob < ApplicationJob
    queue_as :default
    def perform(*args)
        raise NotImplementedError
    end
end