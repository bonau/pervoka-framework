class AddOmniauthToPlayers < ActiveRecord::Migration[5.2]
  def change
    add_column :players, :fb_uid, :string
    add_column :players, :fb_token, :string
    add_column :players, :google_uid, :string
    add_column :players, :google_token, :string
  end
end
