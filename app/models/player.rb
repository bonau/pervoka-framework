class Player < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :google_oauth2]
  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    player = Player.where(:google_token => access_token.credentials.token, :google_uid => access_token.uid).first
    if player
      return player
    else
      existing_player = Player.where(:email => data["email"]).first
      if existing_player
        existing_player.google_uid = access_token.uid
        existing_player.google_token = access_token.credentials.token
        existing_player.save!
        return existing_player
      else
        # Uncomment the section below if you want players to be created if they don't exist
        player = Player.create(
          name: data["name"],
          email: data["email"],
          password: Devise.friendly_token[0,20],
          google_token: access_token.credentials.token,
          google_uid: access_token.uid
        )
      end
    end
  end

  def self.from_omniauth(auth)
    # Case 1: Find existing player by facebook uid
    player = Player.find_by_fb_uid(auth.uid)
    if player
      player.fb_token = auth.credentials.token
      player.save!
      return player
    end
    # Case 2: Find existing player by email
    existing_player = Player.find_by_email(auth.info.email)
    if existing_player
      existing_player.fb_uid = auth.uid
      existing_player.fb_token = auth.credentials.token
      existing_player.save!
      return existing_player
    end
    # Case 3: Create new password
    player = Player.new
    player.fb_uid = auth.uid
    player.fb_token = auth.credentials.token
    player.email = auth.info.email
    player.password = Devise.friendly_token[0,20]
    player.name = auth.info.name
    player.save!
    return player
  end
end
