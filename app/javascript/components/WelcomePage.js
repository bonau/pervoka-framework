import React from "react"
import PropTypes from "prop-types"
import PervokaSocialTool from "./PervokaSocialTool"
import Button from "@material-ui/core/Button"
import AppBar from "@material-ui/core/AppBar"
import { Toolbar, CssBaseline, Card, Grid, withStyles, Paper } from "@material-ui/core"

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3,
    },
  },
});

class WelcomePage extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <CssBaseline />
        <AppBar position="static">
          <Toolbar>
            Pervoka Quiz
          </Toolbar>
        </AppBar>
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            Quiz Generator
          </Paper>
          <PervokaSocialTool facebookAppId={process.env.FACEBOOK_CLIENT_ID}>
          </PervokaSocialTool>
        </main>
      </React.Fragment>
    );
  }
}

WelcomePage.propTypes = {
  username: PropTypes.string
};
export default withStyles(styles)(WelcomePage);
