import React from "react"
import PropTypes from "prop-types"
import { FacebookProvider, Like, Comments } from "react-facebook"
class PervokaSocialTool extends React.Component {
  render() {
    return (
      <React.Fragment>
        <FacebookProvider appId={this.props.facebookAppId} language="zh_TW">
          <Like href="https://pervoka.com" colorScheme="dark" showFaces share />
          <Comments href="https://pervoka.com" />
        </FacebookProvider>
      </React.Fragment>
    );
  }
}

PervokaSocialTool.propTypes = {
  facebookAppId: PropTypes.string
};
export default PervokaSocialTool
