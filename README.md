# Pervoka Framework

The basis used for Pervoka.

## Requirements

* Ruby 2.5.5
* Rails 5.2.3
* NodeJS 10.16.3
* Yarn 1.17.3

## OAuth pre-setup

### Google
[https://console.developers.google.com/apis/credentials](Credentials) in Google Cloud Console

Add "OAuth Client ID"

### Facebook
[https://developers.facebook.com/apps](Apps) in Facebook Developers Console

Add "Facebook Login"

## Usage

```bash
# override per-site settings (optional)
rm -i config/master.key config/credentials.yml.enc
bundle exec rails credentials:edit

# docker image building
bundle exec rails assets:precompile # make sure the assets is precompiled
bundle exec rails assets:clean
bundle exec rails webpacker:clobber
bundle exec rails webpacker:compile
docker image build -t 'asia.gcr.io/pervoka/pervoka-framework' .
docker push asia.gcr.io/pervoka/pervoka-framework

# local development
docker run --rm -it -p 80:3000 -v ${PWD}/app:/pervoka/app -e RAILS_ENV=development asia.gcr.io/pervoka/pervoka-framework bash
# in docker
bundle install --with development
bundle install --with test
bundle exec rails s

# local production run
docker run --rm -it -p 80:3000 -e RAILS_MASTER_KEY=$(cat config/master.key) asia.gcr.io/pervoka/pervoka-framework:0.0.4

# deploy on k8s
kubectl config get-contexts
kubectl config use-context $YOUR_CONTEXT_NAME
kubectl create -f kube/secret.yml
kubectl create -f kube/app.yml
kubectl scale --replicas=2 -f kube/app.yml
kubectl create -f kube/service.yml

# k8s TLS ingress
gcloud compute addresses create YOUR_WEBSITE-ip --global
# make sure the specified domain is mapped to the proper IP generated above (may take a few minutes)
# make sure the name of address is matched with annotation in kube/ingress.yml
# follow the instructions below to setup cert-manager:
#   https://cert-manager.readthedocs.io/en/latest/getting-started/1-configuring-helm.html
kubectl create -f kube/ingress.yml
# and it may take 20 minutes to take effect
```

## Changelog

* 0.0.4
  * devise basic setup
  * omniauth for facebook/google
  * update ruby/rails requirement
* 0.0.3
  * job queue skeleton
* 0.0.2
  * rails production ready
    * reduce docker image building time for smaller change
    * use Rails 5.2 credentials system
    * `FORCE_SSL` env variable is now available
  * reactjs basic layout
  * SEO meta tag support
* 0.0.1
  * basic structure ready for k8s
