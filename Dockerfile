FROM asia.gcr.io/pervoka/minimal-rails:5.2.3

# install node 10.x
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

# install yarn
RUN apt-get install apt-transport-https
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update && apt-get install -y yarn

# generic rails env
ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

WORKDIR /pervoka/

COPY ./Gemfile /pervoka/
COPY ./Gemfile.lock /pervoka/
RUN bundle config --global frozen 1
RUN bundle install --without development test

COPY ./package.json /pervoka/
COPY ./yarn.lock /pervoka/
RUN yarn install

COPY . /pervoka/
EXPOSE 3000
CMD ["bundle", "exec", "rails", "s", "-b", "0.0.0.0", "-p", "3000"]
